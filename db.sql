-- --------------------------------------------------------
-- Host:                         bt236203-001.dbaas.ovh.net
-- Server versie:                10.5.8-MariaDB-1:10.5.8+maria~buster-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Versie:              11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Databasestructuur van sqlbattle wordt geschreven
CREATE DATABASE IF NOT EXISTS `sqlbattle` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sqlbattle`;

-- Structuur van  tabel sqlbattle.battle wordt geschreven
CREATE TABLE IF NOT EXISTS `battle` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `submitted_by` char(36) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `image` text NOT NULL,
  `dataset` text NOT NULL,
  `preview_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`preview_result`)),
  `test_dataset` text NOT NULL,
  `test_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`test_result`)),
  `cache_win_1` char(36) DEFAULT NULL,
  `cache_win_1_score` float DEFAULT NULL,
  `cache_win_2` char(36) DEFAULT NULL,
  `cache_win_2_score` float DEFAULT NULL,
  `cache_win_3` char(36) DEFAULT NULL,
  `cache_win_3_score` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `battle_submitted` (`submitted_by`),
  KEY `battle_win_1` (`cache_win_1`),
  KEY `battle_win_2` (`cache_win_2`),
  KEY `battle_win_3` (`cache_win_3`),
  CONSTRAINT `battle_submitted` FOREIGN KEY (`submitted_by`) REFERENCES `player` (`uuid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `battle_win_1` FOREIGN KEY (`cache_win_1`) REFERENCES `player` (`uuid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `battle_win_2` FOREIGN KEY (`cache_win_2`) REFERENCES `player` (`uuid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `battle_win_3` FOREIGN KEY (`cache_win_3`) REFERENCES `player` (`uuid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporteren was gedeselecteerd

-- Structuur van  tabel sqlbattle.battle_submit wordt geschreven
CREATE TABLE IF NOT EXISTS `battle_submit` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `battle_id` int(4) NOT NULL,
  `player_id` varchar(36) NOT NULL,
  `sql` text NOT NULL,
  `preview_table` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `score` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `submit_battle` (`battle_id`),
  KEY `submit_player` (`player_id`),
  CONSTRAINT `submit_battle` FOREIGN KEY (`battle_id`) REFERENCES `battle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `submit_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporteren was gedeselecteerd

-- Structuur van  tabel sqlbattle.friend wordt geschreven
CREATE TABLE IF NOT EXISTS `friend` (
  `user_sender` char(36) NOT NULL,
  `user_other` char(36) NOT NULL,
  PRIMARY KEY (`user_sender`,`user_other`),
  KEY `friend_other` (`user_other`),
  CONSTRAINT `friend_other` FOREIGN KEY (`user_other`) REFERENCES `player` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friend_sender` FOREIGN KEY (`user_sender`) REFERENCES `player` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporteren was gedeselecteerd

-- Structuur van  tabel sqlbattle.player wordt geschreven
CREATE TABLE IF NOT EXISTS `player` (
  `uuid` char(36) NOT NULL,
  `username` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(256) NOT NULL,
  `image` text DEFAULT NULL,
  `job_title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `git_server` enum('lab','hub','bitbucket') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `git` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hyven` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codepen` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporteren was gedeselecteerd

-- Structuur van  tabel sqlbattle.usage wordt geschreven
CREATE TABLE IF NOT EXISTS `usage` (
  `uuid` varchar(36) NOT NULL,
  `ip` varchar(46) NOT NULL,
  `agent` text DEFAULT NULL,
  `when` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`uuid`,`ip`,`when`) USING BTREE,
  KEY `when` (`when`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporteren was gedeselecteerd

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
