FROM docker.io/maven:3.5-jdk-8 as download-stage
RUN mvn dependency:get -DremoteRepositories=gitlab::default::https://gitlab.com/api/v4/projects/24515761/packages/maven -Dartifact=dev.SQLBattle:SQLBattle:23.4-SNAPSHOT:jar:all -Ddest=./SQLBattle-23.4-SNAPSHOT-all.jar

FROM docker.io/gradle:6.8.3-jdk8
COPY --from=download-stage ./SQLBattle-23.4-SNAPSHOT-all.jar ./SQLBattle-23.4-SNAPSHOT-all.jar
COPY settings.json .
CMD java -jar "SQLBattle-23.4-SNAPSHOT-all.jar"
