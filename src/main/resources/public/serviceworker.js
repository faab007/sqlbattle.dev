const CACHE_NAME = 'sqlbattle-v3';
const CACHE_NAME_AJAX = CACHE_NAME + ".ajax";
const urlsToCache = [
    '/',
    '/index',
    '/self',
    '/signup',
    '/battle',
    '/css/battle.css',
    '/css/content.css',
    '/css/navbar.css',
    '/img/dataset.png',
    '/img/placeholder.png',
    '/js/battle.js',
    '/js/homepage.js',
    '/js/login.js',
    '/js/navbar.js',
    '/js/self.js',
    '/lib/codemirror.js',
    '/lib/codemirror.css',
];
const urlsToNotCache = [
    '/usage',
    '/user',
    '/js/serviceworker-installer.js',
];

const sw = () => {
    self.addEventListener('install', event => {
        registerCache(event, CACHE_NAME, urlsToCache);
        registerCache(event, CACHE_NAME_AJAX, []);
    });

    self.addEventListener('fetch', event => {
        const request = event.request;
        const route = request.url.replace(location.origin, "").split('?')[0];

        if (!shouldUseCache(route)) {
            return;
        }
        event.respondWith((async () => {
            if (isRemoteRoute(route)) {
                return await fetch(request);
            }
            if (isStaticCachedRoute(route)) {
                return caches.match(request, {ignoreSearch: true});
            }
            return await getFastestResult(requestLive(request), requestCache(request));
        })());
    });
};

function isRemoteRoute(route) {
    return route.indexOf('http') === 0;
}

function shouldUseCache(route) {
    return urlsToNotCache.indexOf(route) < 0;
}

function isStaticCachedRoute(route) {
    return urlsToCache.indexOf(route) >= 0;
}

function registerCache(event, cacheName, preloadedURLs) {
    event.waitUntil(
        caches.open(cacheName).then(cache => {
            return cache.addAll(preloadedURLs);
        })
    );
}

function getFastestResult(live, cache) {
    // https://stackoverflow.com/a/37235274
    // It does both requests, the one that succeeded first gets the answer first
    // NOTE: cache should throw error when fails instead of "succeeding" with undefined
    return Promise.all([cache, live].map(p => {
        return p.then(
            val => Promise.reject(val),
            err => Promise.resolve(err)
        );
    })).then(
        errors => Promise.reject(errors),
        val => Promise.resolve(val)
    );
}

function requestLive(request) {
    return fetch(request).then(async response => {
            if (!response || response.status !== 200 || response.type !== 'basic') {
                return response;
            }
            const clone = response.clone();
            await caches.open(CACHE_NAME_AJAX)
                .then(cache => {
                    cache.put(request, response);
                    return response;
                });
            return clone;
        }
    );
}

function requestCache(request) {
    return caches.match(request, {ignoreSearch: true}).then(response => {
        if (response == null) {
            return Promise.reject(response);
        }
        return response;
    });
}

function saveResponse(event, response) {
    if (!response || response.status !== 200 || response.type !== 'basic') {
        return response;
    }
    const responseToCache = response.clone();
    caches.open(CACHE_NAME_AJAX).then(cacheAJAX => {
        cacheAJAX.put(event.request, responseToCache);
    });
    return response;
}

sw();