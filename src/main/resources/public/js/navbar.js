window.thereIsAUpdateListeners = window.thereIsAUpdateListeners || [];

function isResponseError(resonse) {
    return resonse instanceof Array
        && resonse.length > 0
        && resonse[0].indexOf
        && resonse[0].indexOf(': ') >= 0;
}

let uuid = localStorage.getItem('uuid');
if (uuid == null) {
    uuid = (() => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        const r = (Math.random() * 15 + 1) | 0;
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    }))();
    localStorage.setItem('uuid', uuid);
}


// How2Play
$(document).ready(() => {
    const parent = $('#navbarDropdown').parent();
    const wrapper = $(parent.children().get(1));
    wrapper.css('width', '40vw');
    wrapper.css('left', 'calc(-40vw + 130px)');
    wrapper.css('padding', '1em');
    wrapper.html(
        '<h3>How 2 play</h3>' +
        '<b>The objective</b> is to write a fitting answer in the least amount of code as possible.<br><br>' +
        '<b>Rules</b> are that no external code is allowed, keep it within the SQL language.<br><br>' +
        '<b>Usage</b> The editor is realtime, there is no changes made to it between you typing and the result showing. The server will validate on a slightly different engine as you are working on, so you can expect differences. If you use chromium the differences should be kept to a minimal.<br><br>' +
        '<b>Note</b> that comments, new lines and such do count towards the characters'
    );
});

$(document).ready(() => {
    const updateBtn = $('.btn-update');

    window.thereIsAUpdateListeners.push((isThereIsAUpdate, latestVersion, currentVersion) => {
        if (isThereIsAUpdate) {
            updateBtn.removeClass('hidden');
        }
    });

    updateBtn.click(() => {
        updateBtn.addClass('disabled');
        updateBtn.addClass('btn-warning');
        updateBtn.removeClass('btn-info');
        updateSW(() => {
            updateBtn.removeClass('btn-warning');
            updateBtn.addClass('btn-success');
            updateBtn.text('Updated ✔️');
            setTimeout(() => {
                updateBtn.addClass('hidden');
                setTimeout(() => {
                    location.reload();
                }, 500);
            }, 5000);
        }, () => {
            updateBtn.addClass('btn-danger');
            updateBtn.removeClass('btn-warning');
            setTimeout(() => {
                updateBtn.addClass('btn-info');
                updateBtn.removeClass('btn-danger');
                updateBtn.removeClass('warning');
                updateBtn.removeClass('disabled');
            }, 5000);
        });
    });
});

// Challenges on site Latest battles
$(document).ready(() => {
    const element = $('#nav-user');
    const auth = localStorage.getItem('auth');
    const self = localStorage.getItem('self');
    if (auth && self) {
        element.children().remove();

        $.ajax({
            url: '/user',
            method: 'GET',
            headers: {authorization: auth}
        }).done(selfPlayer => {
            if (isResponseError(selfPlayer)) {
                console.error(selfPlayer);
                return;
            }
            if (selfPlayer) {
                localStorage.setItem('self', JSON.stringify(selfPlayer));
            }
        });

        const aElement = $('<a class="nav-link">');
        aElement.attr('href', '/self');
        const selfPlayer = JSON.parse(self);
        aElement.text(selfPlayer.username);
        const imageElement = $('<img>');
        imageElement.attr('alt', selfPlayer.username);
        imageElement.attr('src', selfPlayer.image ? selfPlayer.image : '/img/placeholder.png');
        aElement.prepend(imageElement);
        element.append(aElement);
    }
});


// Navbar adding of battles into the dropdown
$(document).ready(() => {
    const anchor = $('.anchor.nav-dropdown-battles');
    const parent = anchor.parent();

    $.getJSON('/battles', battles => {
        if (isResponseError(battles)) {
            anchor.text('ERROR');
            console.error(battles);
            return;
        }
        anchor.remove();

        battles.forEach(battle => {
            // <li>
            //   <a class="dropdown-item" href="/battle?id=1">
            //     #1 ~
            //     <span>Test1</span>
            //   </a>
            // </li>
            const newLiElement = $('<li>');
            const newAElement = $('<a>');
            const newSpanElement = $('<span>');
            newAElement.addClass('dropdown-item');
            newAElement.attr('href', `/battle?id=${battle.id}`);
            newAElement.text(`#${battle.id} ~ `);
            newSpanElement.text(battle.name);
            newAElement.append(newSpanElement);
            newLiElement.append(newAElement);
            parent.append(newLiElement);
        });
    });
});

// Users online on homepage
$(document).ready(() => {
    const anchor = $('.anchor.users-online');
    const parent = anchor.parent();

    $.getJSON('/usage', {uuid}, users => {
        if (isResponseError(users)) {
            anchor.text('ERROR');
            console.error(users);
            return;
        }

        parent.text(users);
        if (users === 0) {
            parent.parent().children().first().text('🤯')
        }
        if (users === 1) {
            parent.parent().children().last().text('Player online');
        }
    }).fail(function () {
        anchor.text('Offline');
        parent.parent().children().first().text('📶')
        parent.parent().children().last().text('');
    });
});
