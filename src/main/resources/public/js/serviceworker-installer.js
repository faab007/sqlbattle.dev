window.thereIsAUpdateListeners = window.thereIsAUpdateListeners || [];

setTimeout(() => {
    const latestVersion = parseInt(
        "1996" +           // AUTO GENERATED
        "04" +             // AUTO GENERATED
        "23" +             // AUTO GENERATED
        "06"               // AUTO GENERATED
    );                     // AUTO GENERATED

    window.thereIsAUpdateListeners.push((isThereIsAUpdate, latestVersion, currentVersion) => {
        if (isThereIsAUpdate) {
            console.info(`There is a SW update! Current: ${currentVersion} Latest: ${latestVersion}`);
        } else {
            console.info(`SW seems to  be up-to-date. Current: ${currentVersion} Latest: ${latestVersion}`);
        }
    });

    if ('serviceWorker' in navigator) {
        const swStorageKey = 'sw-version';

        navigator.serviceWorker.register('/serviceworker.js').then(registration => {
            if (registration.installing) {
                localStorage.setItem(swStorageKey, JSON.stringify(dateToVersion()));
            }

            const currentVersion = getCurrentSWVersion(swStorageKey);
            const thereIsAUpdate = shouldUpdateSW(currentVersion, latestVersion);
            window.thereIsAUpdateListeners.forEach(callback => {
                try {
                    callback(thereIsAUpdate, latestVersion, currentVersion);
                } catch (e) {
                    console.error(e);
                }
            });
        });
    }
}, 1000);

function updateSW(callbackInstalled, callbackFailed) {
    navigator.serviceWorker.getRegistrations().then(async registrations => {
        await caches.keys().then(async cacheNames => {
            return await Promise.allSettled(cacheNames.map(name => caches.delete(name)));
        });
        await Promise.allSettled(registrations.map(registration => registration.unregister()))
            .then(all => {
                callbackInstalled();
                return all;
            }, error => {
                console.error(error);
                callbackFailed();
                return error;
            });
    })
}

function getCurrentSWVersion(swStorageKey) {
    let swInstalledVersion = localStorage.getItem(swStorageKey);
    swInstalledVersion = JSON.parse(swInstalledVersion);
    if (typeof swInstalledVersion !== "number") {
        swInstalledVersion = dateToVersion();
    }
    return swInstalledVersion;
}

function shouldUpdateSW(swInstalledVersion, latestVersion) {
    return swInstalledVersion <= latestVersion;
}

function dateToVersion() {
    const now = new Date();
    return Number.parseInt(
        "" + now.getFullYear() +
        ((now.getMonth() + 1) < 10 ? "0" + (now.getMonth() + 1) : (now.getMonth() + 1)) +
        (now.getDate() < 10 ? "0" + now.getDate() : now.getDate()) +
        (now.getHours() < 10 ? "0" + now.getHours() : now.getHours())
    );
}