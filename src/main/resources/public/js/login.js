function signInOrUp(form, path) {
    form = $(form);
    const errorDiv = form.find('.alert');

    const username = form.find('input[type=text]').val();
    const password = form.find('input[type=password]').val();

    $.ajax({
        url: '/user/' + path,
        method: 'POST',
        data: {username, password}
    }).done(newPlayer => {
        if (isResponseError(newPlayer)) {
            console.error(newPlayer);
            errorDiv.removeClass('hidden');
            errorDiv.text(newPlayer[0].split(': ')[1]);
            return;
        }
        localStorage.setItem('auth', 'bearer ' + btoa(username + ':' + newPlayer.id))
        localStorage.setItem('self', JSON.stringify(newPlayer));
        location.href = 'self';
    });

    return false;
}