package dev.sqlbattle.backend.general;

public interface ThrowsBiFunction<T1, T2, T3, Ex extends Throwable> {

	T3 accept(T1 t1, T2 t2) throws Ex;
}
