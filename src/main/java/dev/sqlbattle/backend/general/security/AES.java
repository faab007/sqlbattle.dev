package dev.sqlbattle.backend.general.security;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.spec.KeySpec;
import java.util.UUID;

public class AES implements HashFunction {

	private final int size;
	private final String salt;

	public AES(int size, String salt) {
		this.size = size;
		this.salt = salt;
	}

	@Override
	public String apply(String s, UUID u) {
		SecretKey secret;
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			KeySpec spec = new PBEKeySpec(s.toCharArray(), Base64.encodeBase64(salt.getBytes()), 65536, size);
			SecretKey tmp = factory.generateSecret(spec);
			secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
		return new String(Base64.encodeBase64(secret.getEncoded()));
	}
}
