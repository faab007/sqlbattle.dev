package dev.sqlbattle.backend.general.security;

import dev.sqlbattle.backend.general.Settings;

import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class HashSequence {

	private static final Map<Integer, HashFunction> functions;
	private static final String currentOrder = "dqpbdaxmM$";

	static {
		functions = new TreeMap<>();
		functions.put((int) 'q', new SaltPrefix("HYVEN_"));
		functions.put((int) 'p', new SaltPostfix("_PASSWORD"));
		functions.put((int) 'b', new SHA512());
		functions.put((int) 'd', new SHA256());
		functions.put((int) 'a', new MD5(true));
		functions.put((int) 'm', new AES(128, Settings.SETTINGS.getString("encryption.aes128")));
		functions.put((int) 'M', new AES(256, Settings.SETTINGS.getString("encryption.aes256")));
		functions.put((int) 'x', new Base64());
	}


	private static String hashFunction(UUID user, String order, String password) {
		String[] passwordHolder = new String[] { password };
		order.chars().forEach(c -> {
			passwordHolder[0] = functions.getOrDefault(c, (s, u) -> order + s).apply(passwordHolder[0], user);
		});
		return passwordHolder[0];
	}

	public static String hash(UUID user, String password) {
		return hashFunction(user, currentOrder, password);
	}

	public static String hashCustomOrder(UUID user, String password, String order) {
		return hashFunction(user, order, password);
	}

	public static boolean comparePassword(UUID user, String password, String hashPassword) {
		int index = hashPassword.indexOf('$');
		if (index == -1) {
			return false;
		}

		String hashOrder = hashPassword.substring(0, index + 1);
		return hashFunction(user, hashOrder, password).equals(hashPassword);
	}
}
