package dev.sqlbattle.backend.general.security;

import java.util.UUID;

public class Base64 implements HashFunction {

	public String apply(String s) {
		return new String(org.apache.commons.codec.binary.Base64.encodeBase64(s.getBytes()));
	}

	@Override
	public String apply(String s, UUID u) {
		return this.apply(s);
	}
}
