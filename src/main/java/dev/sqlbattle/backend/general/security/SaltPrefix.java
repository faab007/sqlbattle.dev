package dev.sqlbattle.backend.general.security;

import java.util.UUID;

public class SaltPrefix implements HashFunction {

	private final String salt;

	public SaltPrefix(String salt) {
		this.salt = salt;
	}

	@Override
	public String apply(String s, UUID u) {
		return salt + s;
	}
}
