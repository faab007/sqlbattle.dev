package dev.sqlbattle.backend.general;

public interface ThrowsTriFunction<T1, T2, T3, T4, Ex extends Throwable> {

	T4 accept(T1 t1, T2 t2, T3 t3) throws Ex;
}
