package dev.sqlbattle.backend.general;

import com.google.gson.FieldAttributes;

public class ExclusionStrategy implements com.google.gson.ExclusionStrategy {

	private final Class<?> toExclude;

	public ExclusionStrategy(Class<?> toExclude) {
		this.toExclude = toExclude;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getDeclaringClass() == toExclude;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return clazz == toExclude;
	}
}
