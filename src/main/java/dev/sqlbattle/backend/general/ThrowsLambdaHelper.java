package dev.sqlbattle.backend.general;

public class ThrowsLambdaHelper {

	public static <T> T tryGet(ThrowsSupplier<T, Throwable> supplier, T orElse) {
		try {
			return supplier.supply();
		} catch (Throwable ignore) {
		}
		return orElse;
	}
}
