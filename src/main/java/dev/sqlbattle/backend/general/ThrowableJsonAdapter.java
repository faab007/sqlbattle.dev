package dev.sqlbattle.backend.general;

import com.google.gson.*;

import java.lang.reflect.Type;

public class ThrowableJsonAdapter implements JsonSerializer<Throwable>, JsonDeserializer<Throwable> {

	@Override
	public Throwable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		return new Throwable();
	}

	@Override
	public JsonElement serialize(Throwable src, Type typeOfSrc, JsonSerializationContext context) {
		StackTraceElement stackTraceElement = src.getStackTrace()[0];
		JsonObject throwableObject = new JsonObject();
		throwableObject.addProperty("declaringClass", stackTraceElement.getClassName());
		throwableObject.addProperty("methodName", stackTraceElement.getMethodName());
		throwableObject.addProperty("fileName", stackTraceElement.getFileName());
		throwableObject.addProperty("lineNumber", stackTraceElement.getLineNumber());
		if (src.getMessage() != null && !src.getMessage().trim().isEmpty()) {
			throwableObject.addProperty("message", src.getMessage());
		}
		return throwableObject;
	}
}
