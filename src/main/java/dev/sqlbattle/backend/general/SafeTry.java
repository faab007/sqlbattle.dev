package dev.sqlbattle.backend.general;

import java.util.function.Consumer;
import java.util.function.Function;

public class SafeTry {

	public static <Ex extends Throwable> void execute(ThrowsRunnable<Ex> runnable) {
		execute(runnable, ex -> {
		});
	}

	public static <Ex extends Throwable> void execute(ThrowsRunnable<Ex> runnable, Consumer<Throwable> onError) {
		try {
			runnable.run();
		} catch (Throwable throwable) {
			onError.accept(throwable);
		}
	}

	public static <T, Ex extends Throwable> T execute(ThrowsSupplier<T, Ex> supplier) {
		return execute(supplier, ex -> null);
	}

	public static <T, Ex extends Throwable> T execute(ThrowsSupplier<T, Ex> supplier, Function<Throwable, T> onError) {
		try {
			return supplier.supply();
		} catch (Throwable throwable) {
			return onError.apply(throwable);
		}
	}

}
