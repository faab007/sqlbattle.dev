package dev.sqlbattle.backend.general;

import java.util.UUID;
import java.util.function.BiFunction;

public class UUIDSum {

	private static final char[] ORDER = new char[] {
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
	};

	public static UUID add(UUID uuidA, UUID uuidB) {
		return loopTrough(uuidA, uuidB, UUIDSum::add);
	}

	public static UUID minus(UUID uuidA, UUID uuidB) {
		return loopTrough(uuidA, uuidB, UUIDSum::minus);
	}

	private static UUID loopTrough(UUID uuidA, UUID uuidB, BiFunction<Character, Character, Character> transformer) {
		char[] a = uuidA.toString().toCharArray();
		char[] b = uuidB.toString().toCharArray();
		StringBuilder newUUID = new StringBuilder(36);

		for (int i = 0; i < 36; i++) {
			if (a[i] != '-') {
				newUUID.append(transformer.apply(a[i], b[i]));
			} else {
				newUUID.append('-');
			}
		}

		return UUID.fromString(newUUID.toString());
	}

	private static char add(char a, char b) {
		int aIndex = indexOrder(a);
		int bIndex = indexOrder(b);

		int newIndex = (aIndex + bIndex) % ORDER.length;
		return ORDER[newIndex];
	}

	private static char minus(char a, char b) {
		int aIndex = indexOrder(a);
		int bIndex = indexOrder(b);

		int newIndex = aIndex - bIndex;
		if (newIndex < 0) {
			newIndex += ORDER.length;
		}
		return ORDER[newIndex];
	}

	private static int indexOrder(char c) {
		for (int i = 0; i < ORDER.length; i++) {
			if (ORDER[i] == c) {
				return i;
			}
		}
		throw new RuntimeException(c + " IS NOT AN VALID CHARACTER IN UUID");
	}
}
