package dev.sqlbattle.backend.general;

public interface ThrowsConsumer<T, Ex extends Throwable> {

	void accept(T t) throws Ex;

	default <T2> ThrowsFunction<T, T2, Ex> toFunction(T2 functionResponse) {
		return (t) -> {
			accept(t);
			return functionResponse;
		};
	}

	default ThrowsConsumer<T, Ex> andThen(ThrowsConsumer<T, Ex> after) {
		return (T t) -> {
			accept(t);
			after.accept(t);
		};
	}
}
