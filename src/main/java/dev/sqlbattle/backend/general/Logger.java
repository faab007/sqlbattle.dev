package dev.sqlbattle.backend.general;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	private static final boolean logToConsole = System.getProperty("os.name").toLowerCase().contains("win");
	private static final String fileLocation = "./logs/";
	private static final DateFormat fileFormat = new SimpleDateFormat("yyyy-MM-dd-HH' %l.log'");
	private static final DateFormat prefixFormat = new SimpleDateFormat("yyyy-MM-dd | HH:mm:ss.SS");

	static {
		File location = new File(fileLocation);
		if (!location.exists()) {
			location.mkdir();
		}
	}

	private enum Level {
		VERBOSE, INFO, WARN, ERROR, DANGER;

		static Level[] levelsInOrder = new Level[] { VERBOSE, INFO, WARN, ERROR, DANGER };
	}

	public static boolean verbose(String message) {
		return log(Level.VERBOSE, message);
	}

	public static boolean info(String message) {
		return log(Level.INFO, message);
	}

	public static boolean warn(String message) {
		return log(Level.WARN, message);
	}

	public static boolean error(String message) {
		return log(Level.ERROR, message);
	}

	public static boolean danger(String message) {
		return log(Level.DANGER, message);
	}

	public static boolean verbose(Throwable throwable) {
		return log(Level.VERBOSE, throwable);
	}

	public static boolean info(Throwable throwable) {
		return log(Level.INFO, throwable);
	}

	public static boolean warn(Throwable throwable) {
		return log(Level.WARN, throwable);
	}

	public static boolean error(Throwable throwable) {
		return log(Level.ERROR, throwable);
	}

	public static boolean danger(Throwable throwable) {
		return log(Level.DANGER, throwable);
	}

	private static String prefixMessage(Level level, String message) {
		return
				StringHelper.makeCorrectLength(prefixFormat.format(new Date()), 25) + " | " +
						StringHelper.makeCorrectLength(Thread.currentThread().getName(), 12) + " | " +
						StringHelper.makeCorrectLength(level.name(), 7) + " | " +
						message + "\r\n";
	}

	private static boolean log(Level level, Throwable throwable) {
		StringBuilder message = new StringBuilder();
		message.append(throwable.getClass().getName());
		throwable.printStackTrace();
		if (throwable.getMessage() != null) {
			message.append(": ");
			message.append('"');
			message.append(throwable.getMessage().replaceAll("\"", "''"));
			message.append('"');
		}
		for (int i = 0; i < Math.min(15, throwable.getStackTrace().length); i++) {
			message.append(" @ ");
			message.append(throwable.getStackTrace()[i]);
		}

		return log(level, message.toString());
	}

	private static synchronized boolean log(Level level, String message) {
		message = prefixMessage(level, message);

		if (logToConsole) {
			System.out.print(message);
		}

		String logFilePath = fileLocation + fileFormat.format(new Date());
		for (Level levelInOrder : Level.levelsInOrder) {
			File logFile = new File(logFilePath.replace("%l", levelInOrder.name().toLowerCase()));
			try {
				if (!logFile.exists() && !logFile.createNewFile()) {
					throw new FileNotFoundException("creating File " + logFile.getAbsolutePath());
				}
				FileOutputStream fos = new FileOutputStream(logFile, true);
				fos.write(message.getBytes(StandardCharsets.UTF_16));
				fos.close();
			} catch (Exception ex) {
				System.err.println("File " + logFilePath + " @ " + logFile.getAbsolutePath());
				ex.printStackTrace();
				return false;
			}
			if (levelInOrder == level) {
				break;
			}
		}
		return true;
	}
}
