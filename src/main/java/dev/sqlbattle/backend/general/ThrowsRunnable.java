package dev.sqlbattle.backend.general;

public interface ThrowsRunnable<Ex extends Throwable> {

	void run() throws Ex;
}
