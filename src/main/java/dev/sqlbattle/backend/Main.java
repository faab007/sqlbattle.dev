package dev.sqlbattle.backend;

import dev.sqlbattle.backend.database.DatabaseInit;
import dev.sqlbattle.backend.general.Initializer;
import dev.sqlbattle.backend.general.Logger;
import dev.sqlbattle.backend.sqlite.SQLiteInitializer;
import dev.sqlbattle.backend.web.WebRouter;

import java.util.Arrays;

public class Main {

	final static Initializer[] toInit = {
			new DatabaseInit(),
			new SQLiteInitializer(),
			new WebRouter(),
	};

	public static void main(String[] args) {
		Logger.info("Starting up");
		Arrays.stream(toInit).forEach(Initializer::init);
		Logger.info("Initialized");
	}
}
