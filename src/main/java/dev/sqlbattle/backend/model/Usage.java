package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.UUIDStringDateKey;

import java.util.Date;
import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class Usage implements Model<Usage> {

	public KeySet keySet;
	public String agent;

	public Usage(UUID uuid, String ip, String agent, Date when) {
		this.keySet = new KeySet(new UUIDStringDateKey(uuid, ip, when), "uuid", "ip", "when");
		this.agent = agent;
	}

	@Override
	public KeySet getPrimaryKey() {
		return keySet;
	}
}
