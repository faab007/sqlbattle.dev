package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.SQLBuilder;
import dev.sqlbattle.backend.database.primarykey.UUIDStringDateKey;
import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.model.Usage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

public class UsageDAO extends DatabaseAccessObject<Usage> {

	private UsageDAO() {
	}

	@Override
	public String getTableName() {
		return "usage";
	}

	@Override
	public Usage fromResultSet(SafeResultSet resultSet) {
		return new Usage(
				UUIDFromResultSet(resultSet, "uuid"),
				resultSet.getString("ip"),
				resultSet.getString("agent"),
				resultSet.getTimestamp("when")
		);
	}

	public int countSince(Connection connection, String since) throws SQLException {
		return Database.executeSingleResultQuery(
				connection,
				"SELECT COUNT(DISTINCT(CONCAT(LEFT(`uuid`, 8), `ip`))) count FROM `usage` WHERE `when` > ?",
				prepare -> prepare.setString(1, since),
				safeResultSet -> safeResultSet.getInt("count")
		);
	}

	public void removeSince(Connection connection, String since) throws SQLException {
		Database.executeQuery(
				connection,
				"DELETE FROM `usage` WHERE `when` < ?",
				prepare -> prepare.setString(1, since)
		);
	}

	@Override
	public boolean create(Connection connection, Usage model) {
		UUIDStringDateKey primaryKey = model.getPrimaryKey().getKeyAs(UUIDStringDateKey.class);
		UUID uuid = primaryKey.getKeyA();
		String ip = primaryKey.getKeyB();
		String agent = model.agent;
		return Database.executeQuery(
				connection,
				SQLBuilder.create(this.getTableName(), "uuid", "ip", "agent"),
				Database.fillObject(new Object[] { uuid.toString(), ip, agent })
		);
	}
}
