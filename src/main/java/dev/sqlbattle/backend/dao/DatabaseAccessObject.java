package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.model.Model;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.SQLBuilder;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.general.SafeResultSet;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public abstract class DatabaseAccessObject<M extends Model<M>> {

	private static Map<Class<? extends DatabaseAccessObject<?>>, DatabaseAccessObject<?>> daoInstanceMap = new HashMap<>();

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static <DAO extends DatabaseAccessObject<?>> DAO getInstance(Class<DAO> daoClass) {
		return (DAO) daoInstanceMap.computeIfAbsent(daoClass, cls -> {
			try {
				Constructor<? extends DatabaseAccessObject<?>> daoConstructor = cls.getDeclaredConstructor();
				boolean isAccessible = daoConstructor.isAccessible();
				daoConstructor.setAccessible(true);
				DatabaseAccessObject<?> instance = daoConstructor.newInstance();
				daoConstructor.setAccessible(isAccessible);
				return instance;
			} catch (Exception exception) {
				throw new RuntimeException(exception);
			}
		});
	}

	protected static final int ALL = -1;

	public abstract String getTableName();

	public boolean exists(Connection connection, KeySet key) throws SQLException {
		return !findWithKey(connection, key).isEmpty();
	}

	public abstract M fromResultSet(SafeResultSet resultSet);

	public Optional<M> findFirstWithKey(Connection connection, KeySet key) throws SQLException {
		return findWithKey(connection, key)
				.stream()
				.findFirst();
	}

	public String[] getColumns() {
		return new String[0];
	}

	public List<M> findWithKey(Connection connection, KeySet key) throws SQLException {
		return Database.resultToList(connection,
				SQLBuilder.readAll(this.getTableName(), getColumns()) + SQLBuilder.whereUnique(key.getKeyColumns()),
				Database.fillObject(key.getKeyValues()),
				this::fromResultSet
		);
	}

	public List<M> findWithKeyLimitOrderBy(Connection connection, KeySet key, String orderBy, int limit) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readXWhereOrderBy(this.getTableName(), SQLBuilder.whereUnique(key.getKeyColumns()), orderBy, limit, getColumns()),
				Database.fillObject(key.getKeyValues()),
				this::fromResultSet
		);
	}

	public List<M> findWithKeyOrderBy(Connection connection, KeySet key, String orderBy) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readAllWhereOrderBy(this.getTableName(), SQLBuilder.whereUnique(key.getKeyColumns()), orderBy, getColumns()),
				Database.fillObject(key.getKeyValues()),
				this::fromResultSet
		);
	}

	public int count(Connection connection) throws SQLException {
		return Database.executeSingleResultQuery(
				connection,
				SQLBuilder.count(this.getTableName()),
				safeResultSet -> safeResultSet.getInt("count")
		);
	}

	public int countWithKey(Connection connection, KeySet key) throws SQLException {
		return Database.executeSingleResultQuery(
				connection,
				SQLBuilder.countWhere(this.getTableName(), SQLBuilder.whereUnique(key.getKeyColumns())),
				Database.fillObject(key.getKeyValues()),
				safeResultSet -> safeResultSet.getInt("count")
		);
	}

	public List<M> getAll(Connection connection) throws SQLException {
		return getX(connection, ALL);
	}

	public List<M> getX(Connection connection, int limit) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readX(this.getTableName(), limit, getColumns()),
				this::fromResultSet
		);
	}

	public List<M> getXOffset(Connection connection, int limit, int offset) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readXAt(this.getTableName(), limit, offset, getColumns()),
				this::fromResultSet
		);
	}

	public List<M> getAllOrderBy(Connection connection, String order) throws SQLException {
		return getXOrderBy(connection, ALL, order);
	}

	public List<M> getXOrderBy(Connection connection, int limit, String order) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readXWithOrder(this.getTableName(), limit, order(order), getColumns()),
				this::fromResultSet
		);
	}

	public List<M> getXOffsetOrderBy(Connection connection, int limit, int offset, String order) throws SQLException {
		return Database.resultToList(
				connection,
				SQLBuilder.readXAtWithOrder(this.getTableName(), limit, offset, order(order), getColumns()),
				this::fromResultSet
		);
	}

	public boolean save(Connection connection, M model) {
		return Database.executeQuery(
				connection,
				SQLBuilder.update(this.getTableName(), model.getColumns()) + SQLBuilder.whereUnique(model.getPrimaryKey().getKeyColumns()),
				Database.fillObject(model.getValues(), model.getPrimaryKey().getKeyValues())
		);
	}

	public boolean delete(Connection connection, M model) {
		return Database.executeQuery(
				connection,
				SQLBuilder.delete(this.getTableName()) + SQLBuilder.whereUnique(model.getPrimaryKey().getKeyColumns()),
				Database.fillObject(model.getPrimaryKey().getKeyValues())
		);
	}

	public boolean create(Connection connection, M model) {
		return Database.executeQuery(
				connection,
				SQLBuilder.create(this.getTableName(), model.getColumns()),
				Database.fillObject(model.getValues())
		);
	}

	public boolean createIgnore(Connection connection, M model) {
		return Database.executeQuery(
				connection,
				SQLBuilder.createIgnore(this.getTableName(), model.getColumns()),
				Database.fillObject(model.getValues())
		);
	}

	protected String order(String order) {
		if (order == null || order.isEmpty()) {
			return "";
		}
		return " ORDER BY " + order + " ";
	}

	public static UUID UUIDFromResultSet(SafeResultSet resultSet, String field) {
		String uuid = resultSet.getString(field);
		return uuid != null ? UUID.fromString(uuid) : null;
	}
}
