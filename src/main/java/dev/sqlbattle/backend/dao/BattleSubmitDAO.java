package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.model.BattleSubmit;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class BattleSubmitDAO extends DatabaseAccessObject<BattleSubmit> {

    private BattleSubmitDAO() {
    }

    @Override
    public String getTableName() {
        return "battle_submit";
    }

    @Override
    public BattleSubmit fromResultSet(SafeResultSet resultSet) {
        return new BattleSubmit(
                resultSet.getInt("id"),
                resultSet.getInt("battle_id"),
                UUIDFromResultSet(resultSet, "player_id"),
                resultSet.getString("sql"),
                resultSet.getString("preview_table"),
                resultSet.getDouble("score")
        );
    }

    //When people have the same score it will sort on id, this is with the assumption that those are incrementally increased
    public List<BattleSubmit> getLeaderboard(Connection connection, int battle, int limit, int skip) throws SQLException {
        return Database.resultToList(
                connection,
                "select *\n" +
                        "from battle_submit b\n" +
                        "where score = (\n" +
                        "    select max(score)\n" +
                        "    from battle_submit\n" +
                        "    where player_id = b.player_id\n" +
                        "      and battle_id = b.battle_id\n" +
                        ") and battle_id = " + battle + "\n" +
                        "group by player_id\n" +
                        "order by score desc, id\n" +
                        "limit " + limit + "\n" +
                        "offset " + skip,
                this::fromResultSet
        );
    }
}
