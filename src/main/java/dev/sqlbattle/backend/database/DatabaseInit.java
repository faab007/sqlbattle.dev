package dev.sqlbattle.backend.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import dev.sqlbattle.backend.general.Initializer;
import dev.sqlbattle.backend.general.Logger;
import dev.sqlbattle.backend.general.Settings;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Map;

public class DatabaseInit implements Initializer {

	@Override
	public void init() {
		HikariConfig databaseConfig = setupConfig();
		setupDatabaseSource(databaseConfig);
		checkDatasource();
	}

	private void setupDatabaseSource(HikariConfig databaseConfig) {
		Database.dataSource = new HikariDataSource(databaseConfig);
	}

	private void checkDatasource() {
		try {
			Connection connection = Database.dataSource.getConnection();
			Statement statement = connection.createStatement();
			statement.execute("SELECT 1");
			Logger.verbose(" + Database connection successful");
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		Logger.verbose("");
	}

	@SuppressWarnings("unchecked")
	private HikariConfig setupConfig() {
		Logger.verbose("Setting up Database connection");
		HikariConfig databaseConfig = new HikariConfig();
		databaseConfig.setDriverClassName(Settings.SETTINGS.getString("database.connection.driverClassName"));
		databaseConfig.setJdbcUrl(Settings.SETTINGS.getString("database.jdbc"));
		databaseConfig.setUsername(Settings.SETTINGS.getString("database.username"));
		databaseConfig.setPassword(Settings.SETTINGS.getString("database.password"));
		databaseConfig.setAutoCommit(Settings.SETTINGS.getBoolean("database.connection.autoCommit"));
		databaseConfig.setConnectionTimeout(Settings.SETTINGS.getInt("database.connection.connectionTimeout"));
		databaseConfig.setIdleTimeout(Settings.SETTINGS.getInt("database.connection.idleTimeout"));
		databaseConfig.setMaxLifetime(Settings.SETTINGS.getInt("database.connection.maxLifetime"));
		databaseConfig.setMinimumIdle(Settings.SETTINGS.getInt("database.connection.minimumIdle"));
		databaseConfig.setMaximumPoolSize(Settings.SETTINGS.getInt("database.connection.maximumPoolSize"));
		databaseConfig.setPoolName(Settings.SETTINGS.getString("database.connection.poolName"));
		databaseConfig.setConnectionInitSql(Settings.SETTINGS.getString("database.connection.connectionInitSql"));

		Map<String, Object> dataSource = (Map) Settings.SETTINGS.get("database.connection.dataSource");
		dataSource.forEach(databaseConfig::addDataSourceProperty);
		return databaseConfig;
	}

}
