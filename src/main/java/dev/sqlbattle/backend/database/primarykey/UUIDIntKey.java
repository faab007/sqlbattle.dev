package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class UUIDIntKey implements Key {

	private final UUID keyA;
	private final int keyB;

	public UUIDIntKey(UUID keyA, int keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public int getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
