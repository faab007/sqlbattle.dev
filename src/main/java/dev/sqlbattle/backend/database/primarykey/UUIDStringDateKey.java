package dev.sqlbattle.backend.database.primarykey;

import java.util.Date;
import java.util.UUID;

public class UUIDStringDateKey implements Key {

	private final UUID keyA;
	private final String keyB;
	private final Date keyC;

	public UUIDStringDateKey(UUID keyA, String keyB, Date keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public String getKeyB() {
		return keyB;
	}

	public Date getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
