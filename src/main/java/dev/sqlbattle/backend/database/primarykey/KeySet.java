package dev.sqlbattle.backend.database.primarykey;

import java.util.Arrays;
import java.util.UUID;

public class KeySet {

	private final Key key;
	private final String[] keyColumns;

	public KeySet(Key key, String... keyColumns) {
		this.key = key;
		this.keyColumns = keyColumns;

		if (key.getKeys().length != keyColumns.length) {
			throw new RuntimeException("Invalid KeySet");
		}
	}

	public Key getKey() {
		return key;
	}

	@SuppressWarnings("unchecked")
	public <K extends Key> K getKeyAs(Class<K> keyClass) {
		return (K) key;
	}

	public Object[] getKeyValues() {
		return Arrays.stream(key.getKeys())
				.map(key -> {
					if (key instanceof UUID) {
						return key.toString();
					}
					return key;
				})
				.toArray(Object[]::new);
	}

	public String[] getKeyColumns() {
		return keyColumns;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof KeySet)) {
			return false;
		}
		KeySet other = (KeySet) obj;
		return Arrays.equals(this.getKeyValues(), other.getKeyValues())
				&& Arrays.equals(this.getKeyColumns(), other.getKeyColumns());
	}
}
