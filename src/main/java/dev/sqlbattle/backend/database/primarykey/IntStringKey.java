package dev.sqlbattle.backend.database.primarykey;

public class IntStringKey implements Key {

	private final int keyA;
	private final String keyB;

	public IntStringKey(int keyA, String keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public int getKeyA() {
		return keyA;
	}

	public String getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
