package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class IntUUIDStringKey implements Key {

	private final int keyA;
	private final UUID keyB;
	private final String keyC;

	public IntUUIDStringKey(int keyA, UUID keyB, String keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public int getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	public String getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
