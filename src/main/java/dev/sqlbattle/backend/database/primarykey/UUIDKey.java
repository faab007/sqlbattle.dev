package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class UUIDKey implements Key {

	private final UUID key;

	public UUIDKey(UUID key) {
		this.key = key;
	}

	public UUID getKey() {
		return key;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKey() };
	}
}
