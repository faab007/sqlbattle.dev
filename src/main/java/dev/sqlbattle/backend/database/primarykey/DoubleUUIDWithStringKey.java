package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class DoubleUUIDWithStringKey implements Key {

	private final UUID keyA;
	private final UUID keyB;
	private final String keyC;

	public DoubleUUIDWithStringKey(UUID keyA, UUID keyB, String keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	public String getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
