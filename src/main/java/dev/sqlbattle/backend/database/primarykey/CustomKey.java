package dev.sqlbattle.backend.database.primarykey;

public class CustomKey implements Key {

	private final Object[] keys;

	public CustomKey(Object... keys) {
		this.keys = keys;
	}

	@Override
	public Object[] getKeys() {
		return keys;
	}
}
