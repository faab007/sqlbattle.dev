package dev.sqlbattle.backend.database.primarykey;

import java.util.Date;

public class DoubleStringWithTimeKey implements Key {

	private final String keyA;
	private final String keyB;
	private final Date keyC;

	public DoubleStringWithTimeKey(String keyA, String keyB, Date keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public String getKeyA() {
		return keyA;
	}

	public String getKeyB() {
		return keyB;
	}

	public Date getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
