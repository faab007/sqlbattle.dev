package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class DoubleUUIDKey implements Key {

	private final UUID keyA;
	private final UUID keyB;

	public DoubleUUIDKey(UUID keyA, UUID keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
