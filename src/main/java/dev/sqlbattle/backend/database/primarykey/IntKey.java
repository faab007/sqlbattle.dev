package dev.sqlbattle.backend.database.primarykey;


public class IntKey implements Key {

	private final Integer key;

	public IntKey(Integer key) {
		this.key = key;
	}

	public Integer getKey() {
		return key;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKey() };
	}
}
