package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class StringUUIDKey implements Key {

	private final String keyA;
	private final UUID keyB;

	public StringUUIDKey(String keyA, UUID keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public String getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
