package dev.sqlbattle.backend.database.primarykey;

import java.sql.Timestamp;

public class TimestampKey implements Key {

	private final Timestamp key;

	public TimestampKey(Timestamp key) {
		this.key = key;
	}

	public Timestamp getKey() {
		return key;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKey() };
	}
}
