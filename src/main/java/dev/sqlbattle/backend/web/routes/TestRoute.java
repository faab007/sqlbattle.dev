package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

public class TestRoute implements Route {

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.get, "/test")
				.build();
	}

	@Override
	public Object call(HttpMethod method, Request request, Response response) {
		return "TEST";
	}
}
